#!/bin/bash

export $(dbus-launch)
dunst -conf ~/.config/dunst/dunstrc &
nitrogen --restore &
picom &
# lxsession &
xsetroot -cursor_name left_ptr &
syncthing &>/dev/null & 

/usr/bin/pipewire & /usr/bin/wireplumber & /usr/bin/pipewire-pulse & 

~/.config/dwm/status.sh &
~/.local/bin/powermon.sh
syncthing &
