#!/bin/sh
temp() {
	cat /sys/class/thermal/thermal_zone0/temp | dd bs=1 count=2 | cat
}
timeanddate() {
	date +"%A, %B %e %T %Z"
}
bat() {
	echo "$(cat /sys/class/power_supply/BAT0/capacity) ($(cat /sys/class/power_supply/BAT0/status))"
}

while true; do
	xsetroot -name "$(echo "$(temp)°C | $(bat) | $(timeanddate)")"
	sleep 1s;
done
