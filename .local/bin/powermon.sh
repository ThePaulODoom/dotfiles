#!/bin/sh

while true; do 
	power=$(cat /sys/class/power_supply/BAT0/capacity)
	status=$(cat /sys/class/power_supply/BAT0/status)
	[ $power -ge 80 ] && [ $status = "Charging" ] && notify-send "Please unplug"
	[ $power -le 20 ] && [ $status = "Discharging" ] && notify-send "Please plug in"
	sleep 1s;
done
